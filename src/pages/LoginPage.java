package pages;

import java.lang.reflect.ParameterizedType;

import org.openqa.selenium.WebDriver;

public class LoginPage extends AbstractPage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public AbstractPage logInWith(String user, String pass) {

        if ("2".equals(pass)) {
            return new ListPage(driver);
        }

        return new MenuPage(driver);
    }

    public String getErrorMessage() {
        return null;
    }

    public static LoginPage goTo() {
        return new LoginPage(getDriver());
    }

}
