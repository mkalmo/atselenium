package ex8;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import pages.*;

public class Ex8 {

    private static final String USERNAME = "user";
    private static final String CORRECT_PASSWORD = "1";
    private static final String WRONG_PASSWORD = "2";

    private WebDriver driver = new HtmlUnitDriver();

    @After
    public void closeDriver() {
        driver.close();
    }

    @Test
    public void loginFailsWithFalseGredentials() {
        LoginPage loginPage = LoginPage.goTo();

        loginPage.logInWith(USERNAME, WRONG_PASSWORD);

        assertThat(loginPage.getErrorMessage(), is(notNullValue()));
    }

    @Test
    public void loginSucceedsWithCorrectGredentials() {
        LoginPage loginPage = LoginPage.goTo();

        MenuPage menuPage = (MenuPage) loginPage.logInWith(
                USERNAME, "2");

        System.out.println(menuPage);

    }

    @Test
    public void sampleTest() {
        driver.get("http://enos.itcollege.ee/~mkalmo/selenium");

        element("username_box").sendKeys("user");
        element("password_box").sendKeys("1");

        element("log_in_button").click();

        element("show_users_link").click();

        List<WebElement> rows = element("user_list")
                .findElements(By.tagName("div"));

        for (WebElement row : rows) {
            System.out.println(row.getAttribute("username"));
            System.out.println(row.getAttribute("password"));
        }

        System.out.println(driver.getPageSource());
    }

    public WebElement element(String id) {
        return driver.findElement(By.id(id));
    }

}
